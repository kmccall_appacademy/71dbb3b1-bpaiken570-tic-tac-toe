# board.rb

class Board
  attr_accessor :grid

  def initialize(grid = Array.new(3) { Array.new(3) })
    @grid = grid
  end

  def place_mark(pos, mark)
    empty?(pos) ? self[pos] = mark : raise('space is taken')
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    (@grid + columns + diagonals).each do |sub|
      return :X if sub == Array.new(3, :X)
      return :O if sub == Array.new(3, :O)
    end
    nil
  end

  def over?
    return true if @grid.flatten.all? { |e| !e.nil? } || winner
    false
  end

  def diagonals
    diag_one = Array.new(3) { |e| self[[e, e]] }
    diag_two = [self[[0, 2]], self[[1, 1]], self[[2, 0]]]
    [diag_one, diag_two]
  end

  def columns
    col_one = Array.new(3) { |e| self[[e, 0]] }
    col_two = Array.new(3) { |e| self[[e, 1]] }
    col_three = Array.new(3) { |e| self[[e, 2]] }
    [col_one, col_two, col_three]
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end
end
