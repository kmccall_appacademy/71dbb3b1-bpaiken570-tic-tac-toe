class ComputerPlayer
  attr_accessor :name, :board, :mark

  def initialize(name)
    @name = name
  end

  def get_move
    winning_move ? winning_move : possible_moves.sample
  end

  def display(board)
    @board = board
  end

  def winning_move
    possible_moves.each do |pos|
      board[pos] = @mark
      if board.winner
        board[pos] = nil
        return pos
      else
        board[pos] = nil
      end
    end
    nil
  end

  def possible_moves
    arr = []
    @board.grid[0].each_index do |col|
      @board.grid.each_index do |row|
        arr << [row, col] if @board.empty?([row, col])
      end
    end
    arr
  end
end
