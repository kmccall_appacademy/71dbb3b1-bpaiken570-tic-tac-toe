class HumanPlayer
  attr_accessor :name, :mark

  def initialize(name)
    @name = name
  end

  def display(board)
    print "\t"
    board.grid[0].length.times { |col| print "#{col}\t" }
    board.grid.each_index do |row|
      print "\n#{row}\t"
      board.grid[0].each_index do |col|
        print board[[row, col]]
        print "\t"
      end
    end
    print "\n"
  end

  def get_move
    puts 'insert a move in the form of: row, column'
    input = gets.chomp.delete(' ,')
    input.chars.map(&:to_i)
  end
end
